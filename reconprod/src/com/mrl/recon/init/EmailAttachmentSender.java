package com.mrl.recon.init;

import java.net.InetAddress;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.mrl.recon.utils.PropertiesReader;

public class EmailAttachmentSender {

	final static Logger logger = Logger.getLogger(EmailAttachmentSender.class);
	
	private static final String SMTP_HOST_NAME = "smtp.mrlgroup.in";
	private static final String SMTP_AUTH_USER = "mrlalerts@mrlgroup.in";
	private static final String SMTP_AUTH_PWD = "JKH&^#*&(*&*34";

	public void sendEmailWithAttachments(String subject, String[] fileNames, InternetAddress[] toAddresses,
			String content) {
		try {
			Properties props = new Properties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", SMTP_HOST_NAME);
			if (InetAddress.getLocalHost().toString().contains("192.168.1.56")
					|| InetAddress.getLocalHost().toString().contains("192.168.1.88")
					|| "PROD".equalsIgnoreCase(new PropertiesReader().getPropValues("profile.region"))) {
				props.put("mail.smtp.auth", "false");
			} else {
				props.put("mail.smtp.auth", "true");
			}

			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(SMTP_AUTH_USER, SMTP_AUTH_PWD);
				}
			};
			Session session = Session.getInstance(props, auth);

			Message msg = new MimeMessage(session);

			msg.setFrom(new InternetAddress("mrlalerts@mrlgroup.in"));
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());

			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(content);

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			if (fileNames.length > 0) {
				for (String fileName : fileNames) {
					MimeBodyPart attachPart = new MimeBodyPart();
					attachPart.attachFile(fileName);
					multipart.addBodyPart(attachPart);
				}
			}
			msg.setContent(multipart);
			Transport.send(msg);
		} catch (Exception e) {
			logger.error("Error getting in sending via mrl email ", e);
			sendYahooEmail(subject , toAddresses, content);
		}
	}
	
	private void sendYahooEmail(String subject,InternetAddress[] toAddresses, String content) {
        String from = "mrlmerchantonboarding@yahoo.com";
        String pass = "Welcome123$";

        String host = "smtp.mail.yahoo.com";

        Properties properties = System.getProperties();

        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.user", from);
        properties.put("mail.smtp.password", pass);
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(properties);
        try {
              MimeMessage message = new MimeMessage(session);

              message.setFrom(new InternetAddress(from));
             
              message.addRecipients(Message.RecipientType.TO, toAddresses);
              message.setSubject(subject);
              message.setText(content);

              Transport transport = session.getTransport("smtp");
              transport.connect(host, from, pass);
              transport.sendMessage(message, message.getAllRecipients());
              transport.close();
        } catch (Exception e) {
        	logger.error("Error getting in sending via yahooo email ", e);
        }
  }
	
}
