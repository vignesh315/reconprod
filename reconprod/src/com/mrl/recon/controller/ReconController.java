package com.mrl.recon.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mrl.recon.model.Status;
import com.mrl.recon.service.ReconService;

@Controller
public class ReconController {
	@Autowired
	private ReconService reconService;

	@RequestMapping(value = "/reconWelcome", method = { RequestMethod.GET }, headers = {
			"Accept=application/json" }, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public Status reconWelcome(HttpServletRequest request) {
		return reconService.reconWelcome();
	}
}
