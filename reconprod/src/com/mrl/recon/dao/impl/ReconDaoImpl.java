package com.mrl.recon.dao.impl;

import javax.annotation.ManagedBean;

import org.apache.log4j.Logger;

import com.mrl.recon.dao.ReconDao;
import com.mrl.recon.model.Status;
import com.mrl.recon.utils.ApplicationConstants;
@ManagedBean
public class ReconDaoImpl implements ReconDao{
	final static Logger logger = Logger.getLogger(ReconDaoImpl.class);
	Status status = new Status();
	@Override
	public Status reconWelcome() {
		Status status = new Status();
		try {
			status.setStatusCode(0);
			status.setStatusMessage(ApplicationConstants.SUCCESS);
		} catch (Exception e) {
			logger.error("Error getting reconWelcome ", e);
		}finally {
			
		}
		return status;
	}
	

}
